cmake_minimum_required(VERSION 2.8.9)
project (se3interface)
# The version number.
set (se3interface_VERSION_MAJOR 0)
set (se3interface_VERSION_MINOR 1)

# Options
option(TEST_MAIN_INTERFACE "Build test main?" OFF)

# Configuration variables
set (EMULATE_SE3 "0")
set (LOG_LEVEL "LOG_LEVEL_ALL")
#set (LOG_LEVEL "LOG_LEVEL_STD")
#set (LOG_LEVEL "LOG_LEVEL_NO")

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "-Wall -std=c++11")

# configuration file
configure_file (
  "${se3interface_SOURCE_DIR}/config/config.h.in"
  "${se3interface_SOURCE_DIR}/include/config.h"
  )

include_directories(include)
add_subdirectory(libraries/secubetm-libraries)

add_library(se3interface STATIC src/se3interface.cpp)
target_link_libraries(se3interface secubetm-libraries)

if (TEST_MAIN_INTERFACE)
  add_executable(se3interface_test src/main.cpp)
  target_link_libraries(se3interface_test se3interface)
  install(TARGETS se3interface_test DESTINATION /usr/bin)
endif ()
