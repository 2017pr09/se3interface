#include "../include/se3interface.h"
#include <string>

#define TEST_STRING   ("test_encryption")
#define TEST_SIZE     strlen(TEST_STRING)

static uint8_t pin_admin[32] = {
	'a','d','m','i', 'n',0,0,0, 0,0,0,0, 0,0,0,0,
	0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
};

static uint8_t pin_user[32] = {
	'u','s','e','r', 0,0,0,0, 0,0,0,0, 0,0,0,0,
	0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
};

static uint8_t data_key_1[32] = {
	1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4,
	1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4
};

static uint8_t data_key_2[32] = {
	5,6,7,8, 5,6,7,8, 5,6,7,8, 5,6,7,8,
	5,6,7,8, 5,6,7,8, 5,6,7,8, 5,6,7,8
};

static uint8_t data_key_3[32] = {
	0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8,
	0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8
};

bool secube_init(void);
