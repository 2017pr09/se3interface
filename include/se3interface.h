#ifndef SE3INTERFACE_H
  #define SE3INTERFACE_H

// constants
  #define PIN_LEN 32
  #define DATA_KEY_LEN 32

// macros
  #define ENC_SIZE(plain_size)    (((plain_size/SE3_L1_CRYPTOBLOCK_SIZE)+1)*SE3_L1_CRYPTOBLOCK_SIZE)

// libraries
  #include <iostream>
  #include <string.h>
  #include "config.h"
  #include "../libraries/secubetm-libraries/include/aes256.h"
  #include "../libraries/secubetm-libraries/include/crc16.h"
  #include "../libraries/secubetm-libraries/include/L0.h"
  #include "../libraries/secubetm-libraries/include/L1.h"
  #include "../libraries/secubetm-libraries/include/pbkdf2.h"
  #include "../libraries/secubetm-libraries/include/se3c0def.h"
  #include "../libraries/secubetm-libraries/include/se3c1def.h"
  #include "../libraries/secubetm-libraries/include/se3comm.h"
  #include "../libraries/secubetm-libraries/include/se3_common.h"
  #include "../libraries/secubetm-libraries/include/sha256.h"


/******************************************************************************/
/************************************SECUBE************************************/
/******************************************************************************/

  class se3interface {
      int secube_id = -1;
      bool init_success = false;
      bool virtual_se3 = false;
      bool logged_in = false;
      bool time_set = false;
      uint8_t board_serial[SE3_SERIAL_SIZE];
      uint8_t pin_admin[PIN_LEN];
      uint8_t pin_user[PIN_LEN];
      uint8_t data_key_1[DATA_KEY_LEN];
      uint8_t data_key_2[DATA_KEY_LEN];
      uint8_t data_key_3[DATA_KEY_LEN];
      uint8_t *encrypted_data = NULL;
      uint8_t *decrypted_data = NULL;
      se3_disco_it it;
    	se3_device dev;
    	se3_session s;
    	uint32_t session_id = 0;
    public:
      se3interface(int l_secube_id);
      ~se3interface(void);
      bool      interface_init();
      bool      interface_init(bool l_virtual_se3);
      bool      set_time(void);
      void      print_serial_number(void);
      void      print_pin_admin(void);
      void      print_pin_user(void);
      void      print_data_key_1(void);
      void      print_data_key_2(void);
      void      print_data_key_3(void);
      void      set_serial_number(uint8_t* v);
      void      set_pin_admin(uint8_t *l_pin_admin);
      void      set_pin_user(uint8_t *l_pin_user);
      void      set_data_key_1(uint8_t *l_data_key);
      void      set_data_key_2(uint8_t *l_data_key);
      void      set_data_key_3(uint8_t *l_data_key);
      bool      login_admin(void);
      bool      login_user(void);
      bool      logout(void);
      uint8_t*  encrypt_buffer(uint8_t* l_buffer, int l_buffer_size, uint16_t* l_enc_buffer_len);
      uint8_t*  decrypt_buffer(uint8_t* l_buffer, int l_buffer_size, uint16_t* l_dec_buffer_len);
  };

#endif
