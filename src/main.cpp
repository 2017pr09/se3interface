#include "../include/main.h"

using namespace std;

static se3interface *secube = NULL;

int main(int argc, char *argv[])
{
  uint8_t *enc_buffer = 0;
  uint8_t *dec_buffer;
  uint16_t enc_buffer_len = 0;
  uint16_t dec_buffer_len = 0;

  bool ret = secube_init();

  if(ret){
    cout << "Secube init OK" << endl;

    secube->login_user();
    secube->set_time();

    enc_buffer = secube->encrypt_buffer((uint8_t *)TEST_STRING, TEST_SIZE, &enc_buffer_len);
    if(enc_buffer_len > 0){
      cout << "data encrypted - length : " << enc_buffer_len << endl;
      dec_buffer = secube->decrypt_buffer(enc_buffer, enc_buffer_len, &dec_buffer_len);
      if(dec_buffer_len > 0){
        cout << "data decrypted - length : " << dec_buffer_len << endl;
        cout << "result: "<< endl;
        cout << "Test string : " << string(TEST_STRING) << endl;
        cout << "Out  string : " << string((char *)dec_buffer) << endl;
      }
    }

    secube->logout();
  }
  else{
    cout << "No SE3 found!" << endl;
  }

  if(ret){
    delete secube;
  }
  return 0;
}

bool secube_init(void){
  bool ret_value = false;

  secube = new se3interface(1);

  if(secube != NULL){
    #if EMULATE_SE3 == 1
      ret_value = secube->interface_init(true);
    #else
      ret_value = secube->interface_init(false);
    #endif
  }

  if(ret_value){
    secube->set_pin_admin(pin_admin);
    secube->set_pin_user(pin_user);
    secube->set_data_key_1(data_key_1);
    secube->set_data_key_2(data_key_2);
    secube->set_data_key_3(data_key_3);

    secube->print_pin_admin();
    secube->print_pin_user();
    secube->print_data_key_1();
    secube->print_data_key_2();
    secube->print_data_key_3();
  }

  return ret_value;
}
