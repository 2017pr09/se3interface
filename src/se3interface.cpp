#include "../include/se3interface.h"

using namespace std;

/******************************************************************************/
/*******************************SECUBE METHODS*********************************/
/******************************************************************************/
se3interface::se3interface(int l_secube_id){
  secube_id = l_secube_id;

  int i;
  for(i = 0; i < SE3_SERIAL_SIZE; i++) {
    board_serial[i] = 0;
  }
  for(i = 0; i < PIN_LEN; i++) {
    pin_admin[i] = 0;
  }
  for(i = 0; i < PIN_LEN; i++) {
    pin_user[i] = 0;
  }
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_1[i] = 0;
  }
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_2[i] = 0;
  }
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_3[i] = 0;
  }

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: New SEcube - ID: " << secube_id << endl;
  #endif
}

se3interface::~se3interface(void){
  if(logged_in){
    logout();
  }
  if(encrypted_data != NULL){
    free (encrypted_data);
  }
  if(decrypted_data != NULL){
    free (decrypted_data);
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Delete SEcube with ID " << secube_id << endl;
  #endif
}

bool se3interface::interface_init(){
  init_success = false;
  virtual_se3 = false;

  /* local variables */
  uint16_t status = 0xffff;

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Looking for SEcube devices..." << endl;
  #endif

	/* Find SEcube devices */
	L0_discover_init(&it);
  if(L0_discover_next(&it)){
    set_serial_number(it.device_info.serialno);
    # if LOG_LEVEL > LOG_LEVEL_NO
    	cout << "LOG: SEcube found!" << endl << "Info:" << endl;
    	cout << "LOG: Path:\t" << string(it.device_info.path) << endl;
  	  print_serial_number();
    #endif

  	/* Open SEcube device */
  	status = L0_open(&dev, &(it.device_info), SE3_TIMEOUT);
  	if (SE3_OK != status) {
      # if LOG_LEVEL > LOG_LEVEL_NO
  		  cout << "LOG: Failure to open device" << endl;
      #endif
  	}
  	else {
      # if LOG_LEVEL > LOG_LEVEL_NO
  		  cout << "LOG: Device " << secube_id << " is now open" << endl;
      #endif
      init_success = true;
  	}
  }

  return init_success;
}

bool se3interface::interface_init(bool l_virtual_se3){
  init_success = false;
  virtual_se3 = l_virtual_se3;

  /* local variables */
  uint16_t status = 0xffff;

  if(!virtual_se3){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Looking for SEcube devices..." << endl;
    #endif

  	/* Find SEcube devices */
  	L0_discover_init(&it);
    if(L0_discover_next(&it)){
      set_serial_number(it.device_info.serialno);
      # if LOG_LEVEL > LOG_LEVEL_NO
      	cout << "LOG: SEcube found!" << endl << "Info:" << endl;
      	cout << "LOG: Path:\t" << string(it.device_info.path) << endl;
    	  print_serial_number();
      #endif

    	/* Open SEcube device */
    	status = L0_open(&dev, &(it.device_info), SE3_TIMEOUT);
    	if (SE3_OK != status) {
        # if LOG_LEVEL > LOG_LEVEL_NO
    		  cout << "LOG: Failure to open device" << endl;
        #endif
    	}
    	else {
        # if LOG_LEVEL > LOG_LEVEL_NO
    		  cout << "LOG: Device " << secube_id << " is now open" << endl;
        #endif
        init_success = true;
    	}
    }
  }
  else{
    init_success = true;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device " << secube_id << " is now open (EMULATED)" << endl;
    #endif
  }
  return init_success;
}

bool se3interface::set_time(void){
  time_set = false;
  /* local variables */
  uint16_t status = 0xffff;

  cout << "LOG: Setting time for SEcube" << endl;

  if(init_success){
    if(!virtual_se3){
    status = L1_crypto_set_time(&s, (uint32_t)time(0));
      if (SE3_OK != status) {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Failure to set time" << endl;
        #endif
        time_set = false;
        logout();
      }
      else {
        time_set = true;
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Time set" << endl;
        #endif
      }
    }
    else{
      cout << "LOG: Time set (EMULATED)" << endl;
      time_set = true;
    }
  }
  else {
    time_set = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device " << secube_id << " is not initialised yet" << endl;
    #endif
  }
  return time_set;
}

void se3interface::print_serial_number(void){
  int i;
  if(init_success){
    cout << "LOG: Serial Number: ";
	  for(i = 0; i < SE3_SERIAL_SIZE; i++) {
		    cout << board_serial[i];
	  }
    cout << endl;
  }
  else {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device " << secube_id << " is not initialised yet" << endl;
    #endif
  }
}

void se3interface::set_serial_number(uint8_t* v){
  int i;
  if(!init_success){
  	for(i = 0; i < SE3_SERIAL_SIZE; i++) {
  		board_serial[i] = (unsigned)v[i];
  	}
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Setting serial for device " << secube_id << ": " << endl;
      print_serial_number();
    #endif
  }
  else {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device is already initialised!" << endl;
    #endif
  }
}

void se3interface::print_pin_admin(void){
  int i;
  cout << "LOG: Pin admin: ";
  for(i = 0; i < PIN_LEN; i++) {
	    cout << pin_admin[i];
  }
  cout << endl;
}

void se3interface::set_pin_admin(uint8_t *l_pin_admin){
  int i;
  for(i = 0; i < PIN_LEN; i++) {
    pin_admin[i] = (unsigned)l_pin_admin[i];
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Setting pin admin for device " << secube_id << ": " << endl;
    print_pin_admin();
  #endif
}

void se3interface::print_pin_user(void){
  int i;
  cout << "LOG: Pin user: ";
  for(i = 0; i < PIN_LEN; i++) {
	    cout << pin_user[i];
  }
  cout << endl;
}

void se3interface::set_pin_user(uint8_t *l_pin_user){
  int i;
  for(i = 0; i < PIN_LEN; i++) {
    pin_user[i] = (unsigned)l_pin_user[i];
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Setting pin user for device " << secube_id << ": " << endl;
    print_pin_user();
  #endif
}

void se3interface::print_data_key_1(void){
  int i;
  cout << "LOG: Data key 1: ";
  for(i = 0; i < DATA_KEY_LEN; i++) {
	    cout << data_key_1[i];
  }
  cout << endl;
}

void se3interface::set_data_key_1(uint8_t *l_data_key){
  int i;
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_1[i] = (unsigned)l_data_key[i];
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Setting data key 1 for device " << secube_id << ": " << endl;
    print_data_key_1();
  #endif
}

void se3interface::print_data_key_2(void){
  int i;
  cout << "LOG: Data key 2: ";
  for(i = 0; i < DATA_KEY_LEN; i++) {
	    cout << data_key_2[i];
  }
  cout << endl;
}

void se3interface::set_data_key_2(uint8_t *l_data_key){
  int i;
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_2[i] = (unsigned)l_data_key[i];
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Setting data key 2 for device " << secube_id << ": " << endl;
    print_data_key_2();
  #endif
}

void se3interface::print_data_key_3(void){
  int i;
  cout << "LOG: Data key 3: ";
  for(i = 0; i < DATA_KEY_LEN; i++) {
	    cout << data_key_3[i];
  }
  cout << endl;
}

void se3interface::set_data_key_3(uint8_t *l_data_key){
  int i;
  for(i = 0; i < DATA_KEY_LEN; i++) {
    data_key_3[i] = (unsigned)l_data_key[i];
  }
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Setting data key 3 for device " << secube_id << ": " << endl;
    print_data_key_3();
  #endif
}

bool se3interface::login_admin(void){
  bool ret_value = true;
  /* local variables */
  uint16_t status = 0xffff;

  if(!logged_in){
    if(!virtual_se3){
      status = L1_login(&s, &dev, pin_admin, SE3_ACCESS_ADMIN);
      if (SE3_OK != status) {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Failure to log in as admin" << endl;
        #endif
        ret_value = false;
      }
      else {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Logged in as admin" << endl;
        #endif
        logged_in = true;
      }
    }
    else{
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Logged in as admin (EMULATED)" << endl;
      #endif
      logged_in = true;
    }
  }
  else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Alredy logged in" << endl;
    #endif
  }

  return ret_value;
}

bool se3interface::login_user(void){
  bool ret_value = true;
  /* local variables */
  uint16_t status = 0xffff;

  if(!logged_in){
    if(!virtual_se3){
      status = L1_login(&s, &dev, pin_user, SE3_ACCESS_USER);
      if (SE3_OK != status) {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Failure to log in as user" << endl;
        #endif
        ret_value = false;
      }
      else {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Logged in as user" << endl;
        #endif
        logged_in = true;
      }
    }
    else{
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Logged in as user (EMULATED)" << endl;
      #endif
      logged_in = true;
    }
  }
  else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Alredy logged in" << endl;
    #endif
  }

  return ret_value;
}

bool se3interface::logout(void){
  bool ret_value = true;
  /* local variables */
  uint16_t status = 0xffff;

  if(logged_in){
    if(!virtual_se3){
      status = L1_logout(&s);
  		if (SE3_OK != status) {
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Failure to log out" << endl;
        #endif
  			ret_value = false;
  		}
  		else {
        logged_in = false;
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Logged out" << endl;
        #endif
  		}
    }
    else{
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Logged out (EMULATED)" << endl;
      #endif
      logged_in = false;
    }
  }
  else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: No previous log in" << endl;
    #endif
  }
  return ret_value;
}
uint8_t* se3interface::encrypt_buffer(uint8_t* l_buffer, int l_buffer_size, uint16_t* l_enc_buffer_len){
  /* local variables */
  uint16_t status = 0xffff;
  uint8_t *buffer = NULL;

  if(encrypted_data != NULL){
    free(encrypted_data);
  }
  encrypted_data = NULL;
  *l_enc_buffer_len = 0;

  if(init_success){
    if(logged_in){
      if(time_set){
        if(!virtual_se3){
          /* Initialize encryption */
          status = L1_crypto_init(&s, SE3_ALGO_AES, SE3_DIR_ENCRYPT | SE3_FEEDBACK_ECB, 1, &session_id);
          if (SE3_OK != status) {
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Failure initialise crypto session" << endl;
            #endif
            *l_enc_buffer_len = 0;
          }
          else {
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Encryption Session initialised" << endl;
            #endif
            // input buffer must be a multiple of crypto block size -> use of ENC_SIZE macro
            buffer = (uint8_t*) calloc (ENC_SIZE(l_buffer_size), sizeof(uint8_t));
            strncpy((char *)buffer, (char *)l_buffer, l_buffer_size);
            encrypted_data = (uint8_t*) calloc (ENC_SIZE(l_buffer_size), sizeof(uint8_t));
            /* */
            if(buffer != NULL && encrypted_data != NULL){
              /* Encrypt and finalise encryption session */
              status = L1_crypto_update(&s, session_id, SE3_DIR_ENCRYPT | SE3_FEEDBACK_ECB | SE3_CRYPTO_FLAG_FINIT,
                                0, NULL, ENC_SIZE(l_buffer_size), buffer, l_enc_buffer_len, encrypted_data);
              if (SE3_OK != status) {
                # if LOG_LEVEL > LOG_LEVEL_NO
                  cout << "LOG: Failure encrypt data" << endl;
                #endif
                *l_enc_buffer_len = 0;
              }
              else {
                # if LOG_LEVEL > LOG_LEVEL_NO
                  cout << "LOG: Data encrypted" << endl;
                #endif
                if(buffer != NULL){
                  free(buffer);
                }
              }
            }
            else {
              # if LOG_LEVEL > LOG_LEVEL_NO
                cout << "LOG: Failed to allocate memory" << endl;
              #endif
              *l_enc_buffer_len = 0;
            }
          }
        }
        else{
          encrypted_data = (uint8_t*) calloc (l_buffer_size+1, sizeof(uint8_t));
          if(encrypted_data != NULL){
            strcpy((char *)encrypted_data, (char *)l_buffer);
            *l_enc_buffer_len = l_buffer_size;
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Data encrypted (EMULATED)" << endl;
            #endif
          }
          else{
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Failed to allocate memory" << endl;
            #endif
            *l_enc_buffer_len = 0;
          }
        }
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Time is not set" << endl;
        #endif
        *l_enc_buffer_len = 0;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Login before using Secube" << endl;
      #endif
      *l_enc_buffer_len = 0;
    }
  }
  else {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device is not initialised" << endl;
    #endif
    *l_enc_buffer_len = 0;
  }

  return encrypted_data;
}

uint8_t* se3interface::decrypt_buffer(uint8_t* l_buffer, int l_buffer_size, uint16_t* l_dec_buffer_len){
  /* local variables */
  uint16_t status = 0xffff;
  uint8_t *buffer = NULL;

  if(decrypted_data != NULL){
    free(decrypted_data);
  }
  decrypted_data = NULL;
  *l_dec_buffer_len = 0;

  if(init_success){
    if(logged_in){
      if(time_set){
        if(!virtual_se3){
          status = L1_crypto_init(&s, SE3_ALGO_AES, SE3_DIR_DECRYPT | SE3_FEEDBACK_ECB, 1, &session_id);
      		if (SE3_OK != status) {
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Failure initialise crypto session" << endl;
            #endif
            *l_dec_buffer_len = 0;
      		}
      		else {
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Encryption Session initialised" << endl;
            #endif
            buffer = (uint8_t*) calloc (l_buffer_size, sizeof(uint8_t));

            if(buffer != NULL){
          		/* Decrypt and finalise encryption session */
          		status = L1_crypto_update(&s, session_id, SE3_DIR_DECRYPT | SE3_FEEDBACK_ECB | SE3_CRYPTO_FLAG_FINIT,
          			0, NULL, l_buffer_size, l_buffer, l_dec_buffer_len, buffer);
              if (SE3_OK != status) {
                # if LOG_LEVEL > LOG_LEVEL_NO
                  cout << "LOG: Failure to decrypt data" << endl;
                #endif
                *l_dec_buffer_len = 0;
              }
              else {
                # if LOG_LEVEL > LOG_LEVEL_NO
                  cout << "LOG: Data decrypted" << endl;
                #endif
                decrypted_data = (uint8_t*) calloc ((*l_dec_buffer_len + 1), sizeof(uint8_t));
                if(decrypted_data != NULL){
                  strncpy((char *)decrypted_data, (char *)buffer, *l_dec_buffer_len);
                  free(buffer);
                }
                else {
                  # if LOG_LEVEL > LOG_LEVEL_NO
                    cout << "LOG: Failed to allocate memory" << endl;
                  #endif
                  *l_dec_buffer_len = 0;
                }
              }
            }
            else{
              # if LOG_LEVEL > LOG_LEVEL_NO
                cout << "LOG: Failed to allocate memory" << endl;
              #endif
              *l_dec_buffer_len = 0;
            }
      		}
        }
        else{
          decrypted_data = (uint8_t*) calloc (l_buffer_size + 1, sizeof(uint8_t));
          if(decrypted_data != NULL){
            *l_dec_buffer_len = l_buffer_size + 1;
            strncpy((char *)decrypted_data, (char *)l_buffer, *l_dec_buffer_len);
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Data decrypted (EMULATED)" << endl;
            #endif
          }
          else {
            # if LOG_LEVEL > LOG_LEVEL_NO
              cout << "LOG: Failed to allocate memory" << endl;
            #endif
            *l_dec_buffer_len = 0;
          }
        }
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Time is not set" << endl;
        #endif
        *l_dec_buffer_len = 0;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Login before using Secube" << endl;
      #endif
      *l_dec_buffer_len = 0;
    }
  }
  else {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Device is not initialised" << endl;
    #endif
    *l_dec_buffer_len = 0;
  }
  return decrypted_data;
}
